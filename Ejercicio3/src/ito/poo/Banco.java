package ito.poo;

import java.time.LocalDate;

public class Banco {
	public Banco() {
		
		super();
	}
	                       


	public  Banco(long numCuenta, String numCliente, float saldo, LocalDate fechaApertura) {
		super();
		this.numCuenta = numCuenta;
		this.numCliente = numCliente;
		this.saldo = saldo;
		this.fechaApertura = fechaApertura;
	}

	/**
	 * 
	 */
	private long numCuenta;
	/**
	 * 
	 */
	private String numCliente;
	/**
	 * 
	 */
	private float saldo = 0F;
	/**
	 * 
	 */
	private LocalDate fechaApertura = null;
	/**
	 * 
	 */
	private java.time.LocalDate fechaActualizacion = null;

	/**
	 * 
	 * @param cantidad 
	 * @return 
	 */
	public boolean deposito(float cantidad) {
		return false;
	}

	/**
	 * 
	 * @param cantidad 
	 * @return 
	 */
	public boolean retiro (float cantidad) {
		return false;
	}

	public long getNumCuenta() {
		return numCuenta;
	}



	public void setNumCuenta(long numCuenta) {
		this.numCuenta = numCuenta;
	}



	public String getNumCliente() {
		return numCliente;
	}



	public void setNumCliente(String numCliente) {
		this.numCliente = numCliente;
	}



	public float getSaldo() {
		return saldo;
	}



	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}



	public LocalDate getFechaApertura() {
		return fechaApertura;
	}



	public void setFechaApertura(LocalDate fechaApertura) {
		this.fechaApertura = fechaApertura;
	}



	public LocalDate getFechaActualizacion() {
		return fechaActualizacion;
	}



	public void setFechaActualizacion(LocalDate fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}



	@Override
	public String toString() {
		return "cuentaBancaria [numCuenta=" + numCuenta + ", numCliente=" + numCliente + ", saldo=" + saldo
				+ ", fechaApertura=" + fechaApertura + ", fechaActualizacion=" + fechaActualizacion + "]";
	}
}